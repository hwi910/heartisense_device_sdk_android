HeartiSense Device SDK for Android
===========================
안드로이드 기기를 통해 I.M.LAB 의 HeartiSesne BLE 기기를 쉽게 다룰 수 있도록 제작된 SDK 입니다.
이 SDK 를 사용하면 Bluetooth LE 기술에 대한 이해가 없어도 앱을 제작할 수 있습니다.
저장소에 포함된 샘플앱을 통해 기능을 시험해보고, 소스를 통해 사용법을 익히실 수 있습니다.

사용법
-------
[com.imlabworld.android.heartisense_sdk_android.libs][1] 패키지의 파일들을 import 하여 사용합니다.

[HeartiSenseActivity][2] 클래스를 상속받아 Activity를 구현하면 startCheckBLEPermission() 함수를 통해 블루투스가 비활성화 된 기기, 현재 앱에 블루투스 권한을 허용하지 않은 기기를 손쉽게 검사하고 활성화 시키도록 안내할 수 있습니다.

샘플앱의 SplashActivity 참고부분 :
```java
startCheckBLEPermission(new HeartiSenseBLELib.RequestPermissionCallback()
        {
            @Override
            public void onResultReceive(String logString)
            {
                /// 블루투스 퍼미션이 모두 완성되었을 때 호출됨
                CommonLib.getInstance().COMM01_printLog("블루투스 퍼미션 모두 완료", "남기고 싶은 로그값 입력");

                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Intent intent = new Intent(SplashActivity.this,ScannedDeviceListActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        SplashActivity.this.overridePendingTransition(0,0);


                    }
                },3000);



            }
        }, "로그 --> 스플래시 액티비티에서 사용함");
```

[HeartiSenseBLELib 클래스][3]를 사용하여, BLE 기기를 다룰 수 있습니다.
아래와 같이 싱글톤 패턴으로 객체를 로드하여 사용합니다.

```java
HeartiSenseBLELib.getInstance()
```









[Support][0]

**I.M.LAB**


[0]:https://www.imlabworld.com/

[1]:https://bitbucket.org/hwi910/heartisense_device_sdk_android/src/master/app/src/main/java/com/imlabworld/android/heartisense_sdk_android/libs/

[2]:https://bitbucket.org/hwi910/heartisense_device_sdk_android/src/master/app/src/main/java/com/imlabworld/android/heartisense_sdk_android/libs/HeartiSenseActivity.java

[3]:https://bitbucket.org/hwi910/heartisense_device_sdk_android/src/master/app/src/main/java/com/imlabworld/android/heartisense_sdk_android/libs/HeartiSenseBLELib.java
