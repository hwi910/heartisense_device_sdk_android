package com.imlabworld.android.heartisense_sdk_android.adapter;

import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.imlabworld.android.heartisense_sdk_android.R;

import java.util.ArrayList;
import java.util.HashMap;

public class ScannedDeviceListAdapter extends BaseAdapter
{
    Context context;
    ArrayList<HashMap> scannedDeviceResultList;

    public ScannedDeviceListAdapter(Context context)
    {
        this.context = context;
    }

    public void setScannedDeviceListAndNotify(ArrayList<HashMap> scannedDeviceResultList)
    {
        this.scannedDeviceResultList = scannedDeviceResultList;
        this.notifyDataSetChanged();
    }


    @Override
    public int getCount()
    {
        if(this.scannedDeviceResultList != null)
        {
            return this.scannedDeviceResultList.size();
        }
        return 0;
    }

    @Override
    public Object getItem(int i)
    {
        if(this.scannedDeviceResultList != null)
        {
            return this.scannedDeviceResultList.get(i);
        }
        return null;
    }

    @Override
    public long getItemId(int i)
    {
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if(convertView == null)
        {
            convertView = LayoutInflater.from(this.context).inflate(R.layout.cell_scanned_device,parent,false);
        }

        HashMap oneScannedDeviceInfo = this.scannedDeviceResultList.get(position);

        ScanResult oneScannedResult = (ScanResult) oneScannedDeviceInfo.get("ScanResult");

        int rssiValue = oneScannedResult.getRssi();

        TextView tv01_title = convertView.findViewById(R.id.tv01_title);
        TextView tv02_subTitle = convertView.findViewById(R.id.tv02_subTitle);

        tv01_title.setText(oneScannedResult.getDevice().getName());
        tv02_subTitle.setText(""+rssiValue);


        return convertView;
    }
}