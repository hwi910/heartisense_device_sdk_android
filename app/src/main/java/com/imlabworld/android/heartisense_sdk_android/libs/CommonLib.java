package com.imlabworld.android.heartisense_sdk_android.libs;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.widget.ProgressBar;

public class CommonLib
{
    private CommonLib(){}

    private static class CommonLibHolder
    {
        public static final CommonLib instance = new CommonLib();
    }

    /** 싱글턴 객체 로드  */
    public static CommonLib getInstance()
    {
        return CommonLibHolder.instance;
    }


    /** 로그찍기  */
    public void COMM01_printLog(String message, String logString)
    {
        if(logString.equals("error"))
        {
            Log.e("HWI","log : "+logString+", message : "+message);
        }
        else
        {
            Log.d("HWI","log : "+logString+", message : "+message);
        }

    }

    /** 버튼1개짜리 다이얼로그 생성 */
    public void COMM02_showSimpleDialogWithOneButton(Context context, String title, String message, boolean isCancelable, String neutralButtonText, DialogInterface.OnClickListener btnClickListener)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        if(title != null)
        {
            alertDialogBuilder.setTitle(title);
        }


        if(message != null)
        {
            alertDialogBuilder.setMessage(message);
        }

        alertDialogBuilder.setCancelable(isCancelable);
        alertDialogBuilder.setNeutralButton(neutralButtonText,btnClickListener);

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void COMM03_moveActivityWithClearCurrentActivity(Activity currentActivity, Class toActivityClass)
    {
        Intent intent = new Intent(currentActivity,toActivityClass);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        currentActivity.startActivity(intent);
        currentActivity.overridePendingTransition(0,0);
    }







    public float COMM04_01_convertDpToPixel(float dp, Context context){
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float)metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }


    public float COMM04_02_convertPixelsToDp(float px, Context context)
    {
        return px / ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }



    public void COMM05_showSimpleDialogWithTwoButton(Context context, String title, String message, boolean isCancelable, String noBtnTitle, DialogInterface.OnClickListener noBtnClickListener, String yesBtnTitle, DialogInterface.OnClickListener yesBtnClickListener)
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);

        alertDialogBuilder.setTitle(title);
        alertDialogBuilder.setMessage(message);
        alertDialogBuilder.setCancelable(isCancelable);

        alertDialogBuilder.setNegativeButton(noBtnTitle,noBtnClickListener);
        alertDialogBuilder.setPositiveButton(yesBtnTitle,yesBtnClickListener);

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }



}