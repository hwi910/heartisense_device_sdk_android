package com.imlabworld.android.heartisense_sdk_android;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.imlabworld.android.heartisense_sdk_android.adapter.ScannedDeviceListAdapter;
import com.imlabworld.android.heartisense_sdk_android.libs.CommonLib;
import com.imlabworld.android.heartisense_sdk_android.libs.HeartiSenseActivity;
import com.imlabworld.android.heartisense_sdk_android.libs.HeartiSenseBLELib;
import com.kaopiz.kprogresshud.KProgressHUD;

import java.util.ArrayList;
import java.util.HashMap;

public class ScannedDeviceListActivity extends HeartiSenseActivity
{
    public ArrayList<HashMap> scannedDeviceResultList;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scanned_device_list);



        ////  로딩바 띄우기
        final KProgressHUD hud = KProgressHUD.create(this);
        hud.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Loading")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .show();




        ListView scannedDeviceListView = (ListView) findViewById(R.id.scannedDeviceListView);
        final ScannedDeviceListAdapter adapter = new ScannedDeviceListAdapter(ScannedDeviceListActivity.this);
        scannedDeviceListView.setAdapter(adapter);

        /// BLE 기기 퍼미션 체크 진행
        startCheckBLEPermission(new HeartiSenseBLELib.RequestPermissionCallback()
        {
            @Override
            public void onResultReceive(String logString)
            {

                HeartiSenseBLELib.getInstance().HEART02_scanBLEDeviceAndMakeList(ScannedDeviceListActivity.this, new HeartiSenseBLELib.ScannedDeviceListListener()
                {
                    @Override
                    public void onScannedDeviceListChange(ArrayList<HashMap> scannedDeviceResultList)
                    {
                        hud.dismiss();

                        ScannedDeviceListActivity.this.scannedDeviceResultList = scannedDeviceResultList;
                        CommonLib.getInstance().COMM01_printLog("scannedDeviceResultList 확인 : " + scannedDeviceResultList.toString(), "스플래시 액티비티 확인 3ghy8evi");
                        adapter.setScannedDeviceListAndNotify(scannedDeviceResultList);
                    }
                });

            }

        }, "스플래시 액티비티에서 퍼미션 체크 진행");




        scannedDeviceListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {

                /// 스캔 종료 후 커넥션 및 화면이동 진행 필요
                HeartiSenseBLELib.getInstance().HEART03_stopScanBLEDevice();


                /// 선택된 스캔레코드값을 넘겨준다
                HashMap oneScannedDeviceResult = scannedDeviceResultList.get(position);
                Intent intent = new Intent(ScannedDeviceListActivity.this, DeviceDetailActivity.class);
                intent.putExtra("content", oneScannedDeviceResult);
                startActivity(intent);


            }
        });
    }

}
