package com.imlabworld.android.heartisense_sdk_android.libs;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;

import com.imlabworld.android.heartisense_sdk_android.libs.message.BLEMessage;

import org.greenrobot.eventbus.EventBus;

import java.util.List;
import java.util.UUID;

import static com.imlabworld.android.heartisense_sdk_android.libs.HeartiSenseBLELib.UUID00_01_DeviceManufacturerName;
import static com.imlabworld.android.heartisense_sdk_android.libs.HeartiSenseBLELib.UUID01_01_SensorNotificationDistance;
import static com.imlabworld.android.heartisense_sdk_android.libs.HeartiSenseBLELib.UUID01_02_SensorNotificationPress;
import static com.imlabworld.android.heartisense_sdk_android.libs.HeartiSenseBLELib.UUID02_01_Nickname;
import static com.imlabworld.android.heartisense_sdk_android.libs.HeartiSenseBLELib.UUID02_02_Calibration;

public class HeartiSenseGattCallback extends BluetoothGattCallback
{


    public interface HeaertiSenseGattSimpleCallback
    {

        void onGattServiceDistanceAndPressFoundSuccess(BluetoothGatt gatt, BluetoothGattCharacteristic distanceCharacteristic, BluetoothGattCharacteristic pressCharacteristic);
        void onGattServiceFoundFail(BluetoothGatt gatt, String errorMessage);
        void onNotificationReceiveValue01_Distance(int notiValue);
        void onNotificationReceiveValue02_Press(int notiValue);

    }


    HeaertiSenseGattSimpleCallback callback = null;
    BluetoothGattCharacteristic calibrationCharacteristic;






    public HeartiSenseGattCallback(HeaertiSenseGattSimpleCallback callback)
    {
        super();
        this.callback = callback;
    }

    @Override
    public void onPhyUpdate(BluetoothGatt gatt, int txPhy, int rxPhy, int status)
    {
        super.onPhyUpdate(gatt, txPhy, rxPhy, status);
        CommonLib.getInstance().COMM01_printLog("01 onPhyUpdate -->  gatt : " + gatt.toString() + " txPhy : " + txPhy + " rxPhy : " + rxPhy + " status : " + status, "askufna");
    }

    @Override
    public void onPhyRead(BluetoothGatt gatt, int txPhy, int rxPhy, int status)
    {
        super.onPhyRead(gatt, txPhy, rxPhy, status);
        CommonLib.getInstance().COMM01_printLog("02 onPhyRead -->  gatt : " + gatt.toString() + " txPhy : " + txPhy + " rxPhy : " + rxPhy + " status : " + status, "svsee");
    }

    @Override
    public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState)
    {
        super.onConnectionStateChange(gatt, status, newState);

        String newStateString = "";

        if (BluetoothProfile.STATE_CONNECTED == newState)
        {
            /// 기기에 연결되었습니다.
            newStateString = "블루투스 기기 연결";

            /// 서비스 찾기 시작
            gatt.discoverServices();

        }
        else if (BluetoothProfile.STATE_DISCONNECTED == newState)
        {
            /// 기기 연결이 해제되었습니다.
            newStateString = "블루투스 기기 연결 해제";
        }
        else
        {
            /// 기타 상태입니다.
            newStateString = "" + newState;
        }

        CommonLib.getInstance().COMM01_printLog("03 onConnectionStateChange -->  gatt : " + gatt.toString() + " status : " + status + " newState : " + newStateString, "vstgsdf");

    }

    @Override
    public void onServicesDiscovered(BluetoothGatt gatt, int status)
    {
        super.onServicesDiscovered(gatt, status);


        CommonLib.getInstance().COMM01_printLog("04 onServicesDiscovered -->  gatt : " + gatt.toString() + " status : " + status, "aheeraga");




        ///    ------------------------------------   서비스 나열 및 확인    ---------------------------------------    ///


        BluetoothGattCharacteristic deviceManufacturerNameCharacteristic = null;

        BluetoothGattCharacteristic distanceCharacteristic = null;
        BluetoothGattCharacteristic pressCharacteristic = null;

        BluetoothGattCharacteristic nicknameCharacteristic = null;
        calibrationCharacteristic = null;


        List<BluetoothGattService> serviceArr = gatt.getServices();


        /// -----------  BLE 서비스 조회  -------------  ////
        for (int i = 0; i < serviceArr.size(); i++)
        {
            BluetoothGattService oneService = serviceArr.get(i);

            CommonLib.getInstance().COMM01_printLog("서비스 UUID 확인 --> i : " + i + ", ServiceUUID : " + oneService.getUuid().toString() + ", oneService : " + oneService.toString(), "asiufisuh");

            List<BluetoothGattCharacteristic> characteristics = oneService.getCharacteristics();


            for (int j = 0; j < characteristics.size(); j++)
            {
                BluetoothGattCharacteristic oneCharacteristic = characteristics.get(j);
                UUID characteristicUuid = oneCharacteristic.getUuid();
                CommonLib.getInstance().COMM01_printLog("캐릭터 UUID 확인 --> j : " + j + ", characteristicUuid : " + characteristicUuid + " oneCharacteristic : " + oneCharacteristic.toString(), "sgege");



                if (UUID00_01_DeviceManufacturerName.equals(characteristicUuid.toString()))
                {
                    /// 제조사 이름 찾음
                    deviceManufacturerNameCharacteristic = oneCharacteristic;
                }
                else if (UUID01_01_SensorNotificationDistance.equals(characteristicUuid.toString()))
                {
                    /// 거리 서비스 찾음
                    distanceCharacteristic = oneCharacteristic;
                }
                else if (HeartiSenseBLELib.UUID01_02_SensorNotificationPress.equals(characteristicUuid.toString()))
                {
                    /// 압력 서비스 찾음
                    pressCharacteristic = oneCharacteristic;
                }
                else if (UUID02_01_Nickname.equals(characteristicUuid.toString()))
                {
                    /// 닉네임 서비스 찾음
                    nicknameCharacteristic = oneCharacteristic;
                }
                else if (UUID02_02_Calibration.equals(characteristicUuid.toString()))
                {
                    /// 캘리브레이션 서비스 찾음
                    calibrationCharacteristic = oneCharacteristic;
                    HeartiSenseBLELib.getInstance().calibrationCharacteristic = oneCharacteristic;
                }

            }
        }


        ///  거리, 호흡 기술자 가져오기
        List<BluetoothGattDescriptor> distanceDescriptorArr = distanceCharacteristic.getDescriptors();
        List<BluetoothGattDescriptor> pressDescriptorArr = pressCharacteristic.getDescriptors();





        BluetoothGattDescriptor distanceDescriptor = null;
        BluetoothGattDescriptor pressDescriptor = null;




        if (distanceDescriptorArr.size() > 0)
        {
            /// 거리 서비스 기술자 할당
            distanceDescriptor = distanceDescriptorArr.get(0);
        }

        if (pressDescriptorArr.size() > 0)
        {
            /// 호흡 서비스 기술자 할당
            pressDescriptor = pressDescriptorArr.get(0);
        }



        /// 기기이름 읽기
        gatt.readCharacteristic(deviceManufacturerNameCharacteristic);


        if (distanceDescriptor == null)
        {
            CommonLib.getInstance().COMM01_printLog("거리 기술자 없음", "kgrhiwofj");

            this.callback.onGattServiceFoundFail(gatt, "거리 기술자 없음");
        } else if (pressDescriptor == null)
        {
            CommonLib.getInstance().COMM01_printLog("호흡 기술자 없음", "pberjtoj");
            this.callback.onGattServiceFoundFail(gatt, "호흡 기술자 없음");
        }
        else
        {
            CommonLib.getInstance().COMM01_printLog("기술자 전체 검색 완료", "iweghyf");
            this.callback.onGattServiceDistanceAndPressFoundSuccess(gatt, distanceCharacteristic, pressCharacteristic);
        }

    }



    @Override
    public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status)
    {
        super.onCharacteristicRead(gatt, characteristic, status);
        CommonLib.getInstance().COMM01_printLog("05 onCharacteristicRead -->  gatt : " + gatt.toString() + " characteristic : " + characteristic.toString() + " status : " + status, "sdkbhiun");

        if(characteristic.getUuid().toString().equals(UUID00_01_DeviceManufacturerName))
        {
            /// 제조사 이름일 경우
            String readValue = characteristic.getStringValue(0);
            HeartiSenseBLELib.getInstance().deviceInfo01_manufacturerName = readValue;


            BLEMessage message = new BLEMessage();
            message.type = BLEMessage.BLEMessageType.TYPE02_01_onDeviceManufacturerRead;
            message.stringValue = readValue;

            EventBus.getDefault().post(message);

            CommonLib.getInstance().COMM01_printLog("------>   제조사 이름을 읽었습니다 -->  readValue : "+readValue,"asgfqweg");

            /// 이제 호흡 캘리값을 읽어온다.
            gatt.readCharacteristic(calibrationCharacteristic);


        }
        else if(characteristic.getUuid().toString().equals(UUID02_02_Calibration))
        {
            int readValue = HeartiSenseBLELib.getInstance().HEART08_convertByteArrayToInt(characteristic.getValue());
            HeartiSenseBLELib.getInstance().deviceInfo02_pressCaliValue= readValue;

            BLEMessage message = new BLEMessage();
            message.type = BLEMessage.BLEMessageType.TYPE02_02_onDevicePressCaliValueRead;
            message.value = readValue;

            EventBus.getDefault().post(message);

            CommonLib.getInstance().COMM01_printLog("------>   호흡 캘리값을 읽었습니다. -->  readValue : "+readValue,"waigryl");
        }


    }

    @Override
    public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status)
    {
        super.onCharacteristicWrite(gatt, characteristic, status);
        CommonLib.getInstance().COMM01_printLog("06 onCharacteristicWrite -->  gatt : " + gatt.toString() + " characteristic : " + characteristic.toString() + " status: " + status, "shaey");

        if(characteristic.getUuid().toString().equals(UUID02_02_Calibration))
        {
            calibrationCharacteristic = characteristic;
            gatt.readCharacteristic(calibrationCharacteristic);
        }

    }

    @Override
    public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic)
    {
        super.onCharacteristicChanged(gatt, characteristic);


        if (      UUID01_01_SensorNotificationDistance.equals(characteristic.getUuid().toString())
                ||  UUID01_02_SensorNotificationPress.equals(characteristic.getUuid().toString())
                ||  UUID02_01_Nickname.equals(characteristic.getUuid().toString())
                ||  UUID02_02_Calibration.equals(characteristic.getUuid().toString())
                )
        {

            Integer receive_value_int = 0;

            if(UUID01_01_SensorNotificationDistance.equals(characteristic.getUuid().toString()))
            {
                /// 거리값이다.
                receive_value_int = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT8, 0);
                HeartiSenseGattCallback.this.callback.onNotificationReceiveValue01_Distance(receive_value_int);
            }
            else if(UUID01_02_SensorNotificationPress.equals(characteristic.getUuid().toString()))
            {
                /// 호흡값이다.
                receive_value_int = characteristic.getIntValue(BluetoothGattCharacteristic.FORMAT_UINT32, 0);
                HeartiSenseGattCallback.this.callback.onNotificationReceiveValue02_Press(receive_value_int);
            }

        }



    }

    @Override
    public void onDescriptorRead(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status)
    {
        super.onDescriptorRead(gatt, descriptor, status);

        CommonLib.getInstance().COMM01_printLog("-----  onDescriptorRead -->   gatt: "+gatt+"  descriptor : "+descriptor+" status : "+status,"asdfargads");
    }

    @Override
    public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status)
    {
        super.onDescriptorWrite(gatt, descriptor, status);

        CommonLib.getInstance().COMM01_printLog("-----  onDescriptorWrite -->   gatt: "+gatt+"  descriptor : "+descriptor+" status : "+status,"asdfargads");
    }

    @Override
    public void onReliableWriteCompleted(BluetoothGatt gatt, int status)
    {
        super.onReliableWriteCompleted(gatt, status);

        CommonLib.getInstance().COMM01_printLog("-----  onReliableWriteCompleted -->   gatt: "+gatt+" status : "+status,"asdfargads");
    }

    @Override
    public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status)
    {
        super.onReadRemoteRssi(gatt, rssi, status);

    }

    @Override
    public void onMtuChanged(BluetoothGatt gatt, int mtu, int status)
    {
        super.onMtuChanged(gatt, mtu, status);
    }
}
