package com.imlabworld.android.heartisense_sdk_android.libs.message;

import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;

public class BLEMessage
{
    public enum BLEMessageType
    {
        TYPE01_01_onGattServiceDistanceAndPressFoundSuccess,
        TYPE01_02_onGattServiceFoundFail,
        TYPE02_01_onDeviceManufacturerRead,
        TYPE02_02_onDevicePressCaliValueRead,
        TYPE03_01_distanceReceived,
        TYPE03_02_pressReceived
    }


    public BLEMessageType type;
    public int value;
    public String stringValue;


    public BluetoothGatt bleGatt;
    public BluetoothGattCharacteristic distanceCharacteristic;
    public BluetoothGattCharacteristic pressCharacteristic;

    public String errorMessage;

}
