package com.imlabworld.android.heartisense_sdk_android;

import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.le.ScanResult;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.imlabworld.android.heartisense_sdk_android.libs.CommonLib;
import com.imlabworld.android.heartisense_sdk_android.libs.HeartiSenseActivity;
import com.imlabworld.android.heartisense_sdk_android.libs.HeartiSenseBLELib;
import com.imlabworld.android.heartisense_sdk_android.libs.message.BLEMessage;
import com.kaopiz.kprogresshud.KProgressHUD;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.HashMap;

public class DeviceDetailActivity extends HeartiSenseActivity
{

    BluetoothGatt gatt;

    BluetoothGattCharacteristic distanceCharacteristic;
    BluetoothGattCharacteristic pressCharacteristic;


    TextView tv01_01_deviceNameValue;
    TextView tv01_02_deviceManufacturerValue;

    TextView tv02_01_uuid_distance;
    TextView tv02_02_uuid_press;


    TextView tv03_01_value_distance;
    TextView tv03_02_value_press;
    Handler mHandler = new Handler();

    ToggleButton btn01_receiveDistance;
    ToggleButton btn02_receivePress;


    KProgressHUD hud;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device_detail);

        tv01_01_deviceNameValue = findViewById(R.id.tv01_01_deviceNameValue);
        tv01_02_deviceManufacturerValue = findViewById(R.id.tv01_02_deviceManufacturerValue);

        tv02_01_uuid_distance = findViewById(R.id.tv02_01_uuid_distance);
        tv02_02_uuid_press = findViewById(R.id.tv02_02_uuid_press);


        btn01_receiveDistance = findViewById(R.id.btn01_receiveDistance);
        btn02_receivePress = findViewById(R.id.btn02_receivePress);

        tv03_01_value_distance = findViewById(R.id.tv03_01_value_distance);
        tv03_02_value_press = findViewById(R.id.tv03_02_value_press);


        btn01_receiveDistance.setVisibility(View.GONE);
        btn02_receivePress.setVisibility(View.GONE);
        tv03_01_value_distance.setVisibility(View.GONE);
        tv03_02_value_press.setVisibility(View.GONE);



        ////  로딩바 띄우기
        hud = KProgressHUD.create(this);

        hud.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
                .setLabel("Loading")
                .setCancellable(false)
                .setAnimationSpeed(2)
                .show();




        ///  블루투스 퍼미션 체크
        startCheckBLEPermission(new HeartiSenseBLELib.RequestPermissionCallback()
        {
            @Override
            public void onResultReceive(String logString)
            {
                /// 퍼미션 모두 클리어

                HashMap content = (HashMap) getIntent().getSerializableExtra("content");
                ScanResult oneDeviceScannedResult = (ScanResult) content.get("ScanResult");

                final BluetoothDevice oneDevice = oneDeviceScannedResult.getDevice();


                /// 디바이스에 접속 진행
                HeartiSenseBLELib.getInstance().HEART04_connectBluetoothDevice(DeviceDetailActivity.this, oneDevice);


                /// 거리값 받기 토글버튼에 행동 정의
                btn01_receiveDistance.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
                {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                    {
                        if (isChecked)
                        {
                            tv03_01_value_distance.setVisibility(View.VISIBLE);

                        } else
                        {
                            tv03_01_value_distance.setVisibility(View.GONE);
                        }

                        HeartiSenseBLELib.getInstance().HEART09_setNotifyDistanceValueYN(isChecked);


                    }
                });


                /// 호흡값 받기 토글버튼에 행동 정의
                btn02_receivePress.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
                {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked)
                    {
                        if (isChecked)
                        {
                            tv03_02_value_press.setVisibility(View.VISIBLE);

                        } else
                        {
                            tv03_02_value_press.setVisibility(View.GONE);
                        }

                        HeartiSenseBLELib.getInstance().HEART08_setNotifyPressValueYN(isChecked);


                    }
                });

            }
        }, "sdfuwirni");
    }




    ////  ------------------------    EventBus   등록 및 해제  ----------   //////

    @Override
    protected void onStart()
    {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop()
    {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }
    /// --------------------------------------------------------------  //////






    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final BLEMessage message)
    {

        CommonLib.getInstance().COMM01_printLog("--->  메시지 수신 완료    message.type : "+message.type,"akshiglan");

        if(message.type == BLEMessage.BLEMessageType.TYPE01_01_onGattServiceDistanceAndPressFoundSuccess)
        {
            if(hud != null && hud.isShowing())
            {
                hud.dismiss();
            }




            /// 거리, 압력 센서 버튼 표시
            btn01_receiveDistance.setVisibility(View.VISIBLE);
            btn02_receivePress.setVisibility(View.VISIBLE);



            /// 디바이스 이름 표시
            tv01_01_deviceNameValue.setText(HeartiSenseBLELib.getInstance().bleGatt.getDevice().getName());


            tv02_01_uuid_distance.setText(HeartiSenseBLELib.getInstance().distanceCharacteristic.getUuid().toString());
            tv02_02_uuid_press.setText(HeartiSenseBLELib.getInstance().pressCharacteristic.getUuid().toString());

        }
        else if(message.type == BLEMessage.BLEMessageType.TYPE02_01_onDeviceManufacturerRead)
        {
            tv01_02_deviceManufacturerValue.setText(message.stringValue);
        }
        else if(message.type == BLEMessage.BLEMessageType.TYPE03_01_distanceReceived)
        {
            tv03_01_value_distance.setText(""+message.value);
        }
        else if(message.type == BLEMessage.BLEMessageType.TYPE03_02_pressReceived)
        {
            tv03_02_value_press.setText(""+message.value);
        }



    }
}
