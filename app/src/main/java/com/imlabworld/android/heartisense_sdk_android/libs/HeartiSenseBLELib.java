package com.imlabworld.android.heartisense_sdk_android.libs;


import android.Manifest;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanRecord;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

import com.imlabworld.android.heartisense_sdk_android.libs.message.BLEMessage;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**

    EventBus 오픈소스라이브러리를 필수로 사용합니다.




    BLE 이벤트를 받고싶은 곳에 아래함수를 통해 이벤트 수신을 등록/해제 해야합니다.

     ////  ------------------------    EventBus   등록 및 해제  ----------   //////

     @Override
     protected void onStart()
     {
         super.onStart();
         EventBus.getDefault().register(this);
     }

     @Override
     protected void onStop()
     {
         EventBus.getDefault().unregister(this);
         super.onStop();
     }
     /// --------------------------------------------------------------  //////






    아래에 템플릿으로 적힌 onMessageEvent 를 통해 이벤트를 주고받습니다.

    // -------  BLE BroadCast 수신 ------------ //
    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(final BLEMessage message)
    {

    }






*/



public class HeartiSenseBLELib implements HeartiSenseGattCallback.HeaertiSenseGattSimpleCallback
{


    /// GATT UDID (BLE 표준)
    public static final String UUID00_01_DeviceManufacturerName = "00002a29-0000-1000-8000-00805f9b34fb";

    /// GATT UDID ( 하티센스 디바이스 정의된 커스텀 UUID  )
    public static final String UUID01_00_SensorNotificationService = "f000feea-6865-6172-7469-73656e7365ae";
    public static final String UUID01_01_SensorNotificationDistance = "f000feeb-6865-6172-7469-73656e7365ae";
    public static final String UUID01_02_SensorNotificationPress = "f000feec-6865-6172-7469-73656e7365ae";


    public static final String UUID02_01_Nickname = "f0001111-6865-6172-7469-73656e7365ae";
    public static final String UUID02_02_Calibration = "f0001112-6865-6172-7469-73656e7365ae";





    /// 블루투스 기기 허용 권한 요청 상수
    public static final int REQUEST_ENABLE_BT = 1001;
    public static final int REQUEST_PERMISSION_BT = 1002;





    private static class HeartSenseBLELibHolder
    {
        public static final HeartiSenseBLELib instance = new HeartiSenseBLELib();
    }


    /** 싱글턴 인스턴스 로드 */
    public static HeartiSenseBLELib getInstance()
    {
        return HeartSenseBLELibHolder.instance;
    }



    ////  ---------    BLE 사용을 위한 멤버변수   ----------  ////
    BluetoothLeScanner mBLEScanner;
    ScanCallback mScanCallback;




    /// 스캔된 디바이스 리스트 저장
    public ArrayList<HashMap> scannedDeviceResultList;

    ///  선택된 디바이스 검색결과 저장
    public ScanResult selectedScanResult;


    /// gatt, 거리센서, 압력센서 객체 저장
    public BluetoothGatt bleGatt;
    public BluetoothGattCharacteristic distanceCharacteristic;
    public BluetoothGattCharacteristic pressCharacteristic;
    public BluetoothGattCharacteristic calibrationCharacteristic;



    public String deviceInfo01_manufacturerName;
    public int deviceInfo02_pressCaliValue;




    ////  ---------    호흡 캘리브레이션 관련 변수     ---------   ////
    public boolean cali_value00_isIncreasingAtm = true;
    public int cali_value01_prevMaxAtm = 0;
    public int cali_value02_maxAtm = 100;
    public int cali_value03_minAtm = 100;
    public int cali_value04_sumAtm = 0;
    public int cali_value05_countAtm = 0;

    public int cali_value06_01_range_base = 0;
    public int cali_value06_02_range_max = 0;




    ///  ---------------------------------------------  BLE 사용 퍼미션 관련   -----------------------------------------  ///

    /** BLE 퍼미션 요청 후 사용자 응답 시 콜백 */
    public interface RequestPermissionCallback
    {
        void onResultReceive(String logString);
    }

    /**

     블루투스 및 블루투스 LE 기능을 사용하기 위한 모든 체크를 진행하고, 안될경우 대응책을 진행한다.
     액티비티에서 대응책 콜백을 위해 아래 메소드를 추가해야한다.
     (  일반적인 경우 HeartiSenseActivity  에 아래 내용이 구현되어있으므로, HeartiSenseActivity를 상속받고 startCheckBLEPermission() 을 실행하면 된다. )


     @Override
     protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
     {
     super.onActivityResult(requestCode, resultCode, data);


     CommonLib.getInstance().COMM01_printLog("onActivityResult -->  requestCode : " + requestCode + "  resultCode : " + resultCode + "  data : " + data,"스플래시 액티비티에서 블루투스 기기사용 설정 퍼미션 확인");

     if(requestCode == REQUEST_ENABLE_BT)
     {
     checkPermission();
     }

     }


     @Override
     public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
     {
     super.onRequestPermissionsResult(requestCode, permissions, grantResults);

     CommonLib.getInstance().COMM01_printLog("onRequestPermissionsResult --> requestCode : " + requestCode + "  permissions : " + permissions.toString() + "  grantResults : " + grantResults.toString(),"스플래시 액티비티에서 퍼미션 확인");

     if(requestCode == REQUEST_PERMISSION_BT)
     {
     checkPermission();
     }

     }

     */
    public void HEART01_checkBLEPermissionAndRequestEndless(final Activity activity, RequestPermissionCallback callback, String logString)
    {

        CommonLib lib = CommonLib.getInstance();

        /// 블루투스 LE 사용가능성 확인
        if (!activity.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE))
        {
            /// 사용 못하는 경우

            lib.COMM01_printLog("블루투스 LE를 사용할 수 없는 기기", logString);
            CommonLib.getInstance().COMM02_showSimpleDialogWithOneButton(activity, "문제발생", "블루투스 LE를 사용할 수 없는 기기입니다.\n앱을 종료합니다.", false, "앱 종료", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialogInterface, int i)
                {
                    activity.finishAndRemoveTask();
                }
            });
            return;

        } else
        {
            /// 사용할 수 있는 경우

            lib.COMM01_printLog("BLE 사용가능 +_+", logString);


            /// 블루투스 어댑터 초기화
            final BluetoothManager bluetoothManager = (BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE);
            BluetoothAdapter mBluetoothAdapter = bluetoothManager.getAdapter();


            if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled())
            {

                lib.COMM01_printLog("사용자가 블루투스를 활성화하지 않은 경우", logString);

                Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);


            }
            else
            {
                if (mBLEScanner == null)
                {
                    mBLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
                }


                // Checks if Bluetooth LE Scanner is available.
                if (mBLEScanner == null)
                {

                    lib.COMM01_printLog("BLE 스캐너를 사용할 수 없습니다.(롤리팝이상일 경우 BLE 스캐너)", logString);

                    CommonLib.getInstance().COMM02_showSimpleDialogWithOneButton(activity, "문제발생", "블루투스 LE 스캔을 사용할 수 없는 기기입니다.\n앱을 종료합니다.", false, "앱 종료", new DialogInterface.OnClickListener()
                    {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i)
                        {
                            activity.finishAndRemoveTask();
                        }
                    });

                    return;
                }


                String[] requiredPermission = {
                        Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.ACCESS_FINE_LOCATION
                };


                /// 사용자에게 BLE 기능 사용 허용 요청
                boolean permissionCheck = hasPermissions(activity, requiredPermission);

                if (!permissionCheck)
                {
                    lib.COMM01_printLog("앱에서 블루투스 사용권한 없음", logString);
                    ActivityCompat.requestPermissions(activity, requiredPermission, REQUEST_PERMISSION_BT);
                    return;

                } else
                {
                    /// 사용자가 BLE를 허용한 상태이다.
                    lib.COMM01_printLog("블루투스 디바이스 스캔 시작! --> mBluetoothAdapter : " + mBluetoothAdapter, logString);
                    callback.onResultReceive(logString);

                }
            }
        }
    }









    ///  ---------------------------------------------  BLE 스캔 및 리스트 만들기   -----------------------------------------  ///


    public interface ScannedDeviceListListener
    {
        void onScannedDeviceListChange(ArrayList<HashMap> scannedDeviceResultList);
    }


    /// 블루투스 디바이스를 검색하여 리스트를 만들어 리턴해줍니다.
    public void HEART02_scanBLEDeviceAndMakeList(final Activity activity, final ScannedDeviceListListener listener)
    {

        final BluetoothManager bluetoothManager = (BluetoothManager) activity.getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter mBluetoothAdapter = bluetoothManager.getAdapter();


        if (mBLEScanner == null)
        {
            mBLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
        }

        if (mBLEScanner == null)
        {
            CommonLib.getInstance().COMM01_printLog("BLE 스캐너 사용못하는 경우", "asduhasdf");
            return;
        }




        if (scannedDeviceResultList == null)
        {
            scannedDeviceResultList = new ArrayList<>();
        }


        mScanCallback = new ScanCallback()
        {
            @Override
            public void onScanResult(int callbackType, ScanResult result)
            {
                super.onScanResult(callbackType, result);


                BluetoothDevice device = result.getDevice();
                ScanRecord record = result.getScanRecord();

                if (device != null && record != null)
                {
                    Map<Integer, String> parsedData = ParseRecord(record.getBytes());

                    Log.d("HWI","스캔레코드 파싱 --> parsedData : "+parsedData.toString());

                    if (parsedData != null && parsedData.containsKey(20))
                    {
                        String serviceID = parsedData.get(20);

                        if ("FEEA".equals(serviceID))
                        {
                            boolean isExist = false;
                            for (int i = 0; i < scannedDeviceResultList.size(); i++)
                            {
                                HashMap oneHashMap = scannedDeviceResultList.get(i);

                                ScanResult oneScanResultInArr = (ScanResult) oneHashMap.get("ScanResult");

                                String deviceIDInArr = oneScanResultInArr.getDevice().toString();
                                String deviceID = device.toString();

                                if (deviceIDInArr.equals(deviceID))
                                {
                                    /// 동일한 디바이스가 있을 경우 교체한다.
                                    isExist = true;

                                    HashMap oneDeviceResult = new HashMap();
                                    oneDeviceResult.put("ScanResult", result);
                                    scannedDeviceResultList.set(i, oneDeviceResult);
                                    break;
                                }

                            }

                            if (!isExist)
                            {
                                /// 해당 디바이스가 존재하지 않으므로 추가한다.
                                HashMap oneDeviceResult = new HashMap();
                                oneDeviceResult.put("ScanResult", result);
                                scannedDeviceResultList.add(oneDeviceResult);
                            }
                            listener.onScannedDeviceListChange(scannedDeviceResultList);
                        }

                    }

                }

            }


            @Override
            public void onBatchScanResults(List<ScanResult> results)
            {
                super.onBatchScanResults(results);
                CommonLib.getInstance().COMM01_printLog("onBatchScanResults ---> results : " + results, "3h358g7h");
            }

            @Override
            public void onScanFailed(int errorCode)
            {
                super.onScanFailed(errorCode);
                CommonLib.getInstance().COMM01_printLog("BLE 스캔 실패한 경우  --> errorCode : " + errorCode, "dskashgyeri");

            }
        };

        mBLEScanner.startScan(mScanCallback);
    }


    /**  BLE 디바이스 찾기를 중지한다 */
    public void HEART03_stopScanBLEDevice()
    {
        if (mBLEScanner != null && mScanCallback != null)
        {
            mBLEScanner.stopScan(mScanCallback);
        }

    }




    ///  ---------------------------------------------  GATT 서비스 연결 및 콜백 정의   -----------------------------------------  ///

    /** 블루투스 기기에 연결 -> 성공시 GATT 사용가능 */
    public void HEART04_connectBluetoothDevice(final Context context, final BluetoothDevice device)
    {
        device.connectGatt(context,false,new HeartiSenseGattCallback(this));
    }


    public void HEART08_setNotifyPressValueYN(boolean isWantNotify)
    {
        setNotificationReceiveYN(isWantNotify,pressCharacteristic);
    }


    public void HEART09_setNotifyDistanceValueYN(boolean isWantNotify)
    {
        setNotificationReceiveYN(isWantNotify,distanceCharacteristic);
    }





    public void HEART06_initCaliValues()
    {
        cali_value00_isIncreasingAtm = true;
        cali_value01_prevMaxAtm = 0;
        cali_value02_maxAtm = 100;
        cali_value03_minAtm = 100;
        cali_value04_sumAtm = 0;
        cali_value05_countAtm = 0;
        cali_value06_01_range_base = 0;
        cali_value06_02_range_max = 0;
    }




    public boolean HEART07_setPressCaliValue(byte[] caliData)
    {
        boolean isValueSetComplete = calibrationCharacteristic.setValue(caliData);
        boolean isWriteSuccess = bleGatt.writeCharacteristic(calibrationCharacteristic);

        CommonLib.getInstance().COMM01_printLog("------      압력값 캘리 입력 확인 ->   isValueSetComplete : "+isValueSetComplete+"  isWriteSuccess : "+isWriteSuccess,"akwhgi");

        if(isValueSetComplete && isWriteSuccess)
        {
            return true;
        }
        else
        {
            return false;
        }
    }


    public int HEART08_convertByteArrayToInt(byte[] byteValue)
    {
        int value= 0;
        for(int i=0; i<byteValue.length; i++)
            value = (value << 8) | byteValue[i];
        return value;
    }























    // ------------------------------------------------       GATT  서비스  콜백        ------------------------------------------------ /////


    @Override
    public void onGattServiceDistanceAndPressFoundSuccess(BluetoothGatt gatt, BluetoothGattCharacteristic distanceCharacteristic, BluetoothGattCharacteristic pressCharacteristic)
    {

        HeartiSenseBLELib.this.bleGatt = gatt;
        HeartiSenseBLELib.this.distanceCharacteristic = distanceCharacteristic;
        HeartiSenseBLELib.this.pressCharacteristic = pressCharacteristic;



        BLEMessage makedMessage = new BLEMessage();
        makedMessage.bleGatt = gatt;
        makedMessage.distanceCharacteristic = distanceCharacteristic;
        makedMessage.pressCharacteristic = pressCharacteristic;
        makedMessage.type = BLEMessage.BLEMessageType.TYPE01_01_onGattServiceDistanceAndPressFoundSuccess;

        EventBus.getDefault().post(makedMessage);
    }

    @Override
    public void onGattServiceFoundFail(BluetoothGatt gatt, String errorMessage)
    {
        HeartiSenseBLELib.this.bleGatt = gatt;

        CommonLib.getInstance().COMM01_printLog("-----   접속실패 에러메시지 표시   ------  errorMessage : "+errorMessage,"ㅈ호ㅑ");

        BLEMessage makedMessage = new BLEMessage();
        makedMessage.bleGatt = gatt;
        makedMessage.errorMessage = errorMessage;
        makedMessage.type = BLEMessage.BLEMessageType.TYPE01_02_onGattServiceFoundFail;

        EventBus.getDefault().post(makedMessage);
    }

    @Override
    public void onNotificationReceiveValue01_Distance(int notiValue)
    {
        BLEMessage makedMessage = new BLEMessage();
        makedMessage.type = BLEMessage.BLEMessageType.TYPE03_01_distanceReceived;
        makedMessage.value = notiValue;

        EventBus.getDefault().post(makedMessage);
    }

    @Override
    public void onNotificationReceiveValue02_Press(int notiValue)
    {
        BLEMessage makedMessage = new BLEMessage();
        makedMessage.type = BLEMessage.BLEMessageType.TYPE03_02_pressReceived;
        makedMessage.value = notiValue;

        EventBus.getDefault().post(makedMessage);
    }




    // --------------------------------------------------------------------------------------------------------------------------------------------------------------- /////

















    private void setNotificationReceiveYN(boolean wantReceive, BluetoothGattCharacteristic characteristic)
    {
        if(characteristic != null)
        {
            BluetoothGattDescriptor descriptor = characteristic.getDescriptors().get(0);
            bleGatt.setCharacteristicNotification(characteristic, wantReceive); /// 노티피케이션을 (받는/안받는)다고 설정
            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE); /// 노티피케이션 활성화
            bleGatt.writeDescriptor(descriptor); // 노티피케이션 설정 셋팅
        }

    }







    /// 스캔레코드를 파싱합니다. (FEEA 값을 찾을 수 있습니다. Github의  BLEBase)
    // (https://github.com/DrJukka/BLETestStuff/blob/master/MyBLETest/app/src/main/java/org/thaliproject/p2p/mybletest/BLEBase.java)
    private Map<Integer, String> ParseRecord(byte[] scanRecord)
    {
        Map<Integer, String> ret = new HashMap<Integer, String>();
        int index = 0;
        while (index < scanRecord.length)
        {
            int length = scanRecord[index++];

            //Zero value indicates that we are done with the record now
            if (length == 0) break;

            int type = scanRecord[index];
            //if the type is zero, then we are pass the significant section of the data,
            // and we are thud done
            if (type == 0) break;

            byte[] data = Arrays.copyOfRange(scanRecord, index + 1, index + length);

            if (data != null && data.length > 0)
            {
                StringBuilder hex = new StringBuilder(data.length * 2);
                // the data appears to be there backwards
                for (int bb = data.length - 1; bb >= 0; bb--)
                {
                    hex.append(String.format("%02X", data[bb]));
                }
                ret.put(type, hex.toString());
            }
            index += length;
        }

        return ret;
    }


    private boolean hasPermissions(Context context, String... permissions)
    {
        if (context != null && permissions != null)
        {
            for (String permission : permissions)
            {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED)
                {
                    return false;
                }
            }
        }
        return true;
    }
}
