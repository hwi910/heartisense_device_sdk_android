package com.imlabworld.android.heartisense_sdk_android;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.ImageView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.imlabworld.android.heartisense_sdk_android.libs.CommonLib;
import com.imlabworld.android.heartisense_sdk_android.libs.HeartiSenseActivity;
import com.imlabworld.android.heartisense_sdk_android.libs.HeartiSenseBLELib;


/*
   HeartiSenseActivity 를 상속받습니다.

*/
public class SplashActivity extends HeartiSenseActivity
{
    public boolean isFirstRun = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        startCheckBLEPermission(new HeartiSenseBLELib.RequestPermissionCallback()
        {
            @Override
            public void onResultReceive(String logString)
            {
                /// 블루투스 퍼미션이 모두 완성되었을 때 호출됨
                CommonLib.getInstance().COMM01_printLog("블루투스 퍼미션 모두 완료", "is7ghreihf");

                new Handler().postDelayed(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        Intent intent = new Intent(SplashActivity.this,ScannedDeviceListActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(intent);
                        SplashActivity.this.overridePendingTransition(0,0);


                    }
                },3000);



            }
        }, "로그 --> 스플래시 액티비티에서 사용함");


        final ImageView imageView01_logo = findViewById(R.id.imageView01_logo);
        final ImageView imageView02_bottomLogo = findViewById(R.id.imageView02_bottomLogo);

        imageView01_logo.setVisibility(View.INVISIBLE);
        imageView02_bottomLogo.setVisibility(View.INVISIBLE);

        new Handler().postDelayed(new Runnable()
        {
            @Override
            public void run()
            {
                imageView01_logo.setVisibility(View.VISIBLE);
                imageView02_bottomLogo.setVisibility(View.VISIBLE);

                YoYo.with(Techniques.FadeInDown).duration(1500).pivot(YoYo.CENTER_PIVOT, YoYo.CENTER_PIVOT).interpolate(new AccelerateDecelerateInterpolator()).playOn(imageView01_logo);
                YoYo.with(Techniques.FadeInUp).duration(1500).pivot(YoYo.CENTER_PIVOT, YoYo.CENTER_PIVOT).interpolate(new AccelerateDecelerateInterpolator()).playOn(imageView02_bottomLogo);
            }
        },500);


    }



}
