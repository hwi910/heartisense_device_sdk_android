package com.imlabworld.android.heartisense_sdk_android.libs;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;

import com.imlabworld.android.heartisense_sdk_android.R;

import java.util.ArrayList;
import java.util.HashMap;

import static com.imlabworld.android.heartisense_sdk_android.libs.HeartiSenseBLELib.REQUEST_ENABLE_BT;
import static com.imlabworld.android.heartisense_sdk_android.libs.HeartiSenseBLELib.REQUEST_PERMISSION_BT;

public class HeartiSenseActivity extends AppCompatActivity
{
    ArrayList<HashMap> scannedDeviceList;

    HeartiSenseBLELib.RequestPermissionCallback callback;
    String logString;

    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
    }



    private void checkBLEPermission()
    {
        HeartiSenseBLELib hlib = HeartiSenseBLELib.getInstance();
        hlib.HEART01_checkBLEPermissionAndRequestEndless(this,this.callback,this.logString);
    }





    ///  ----------------------------------------  퍼블릭 메소드 시작    ----------------------------------------------------------  ////

    public void startCheckBLEPermission(HeartiSenseBLELib.RequestPermissionCallback callback, String logString)
    {
        this.callback = callback;
        this.logString = logString;
        checkBLEPermission();

    }


    public void hideActionBar()
    {
        getSupportActionBar().hide();
    }




    public void changeActionBarTitle(String title)
    {
        SpannableString titleSpannableString = new SpannableString(title);
        titleSpannableString.setSpan(new ForegroundColorSpan(getResources().getColor(R.color.cigrey)), 0, title.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        getSupportActionBar().setTitle(titleSpannableString);
    }






    ///  ----------------------------------------  액티비티 라이프사이클 관련    ----------------------------------------------------------  ////
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);


        CommonLib.getInstance().COMM01_printLog("onActivityResult -->  requestCode : " + requestCode + "  resultCode : " + resultCode + "  data : " + data, "액티비티에서 블루투스 기기사용 설정 퍼미션 확인");

        if (requestCode == REQUEST_ENABLE_BT)
        {
            checkBLEPermission();
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        CommonLib.getInstance().COMM01_printLog("onRequestPermissionsResult --> requestCode : " + requestCode + "  permissions : " + permissions.toString() + "  grantResults : " + grantResults.toString(), "액티비티에서 퍼미션 확인");

        if (requestCode == REQUEST_PERMISSION_BT)
        {
            checkBLEPermission();
        }

    }

}
